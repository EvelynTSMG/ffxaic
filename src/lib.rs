//! This crate provides a lexer, parser, and compiler for the community-developed Final Fantasy X AI scripting language.
//!
//! The lexer and parser are hand-written using [`this guide`].
//! The compiler is also written manually and probably quite slow.
//! Merge requests are welcome!
//!
//! [`this guide`]: https://domenicquirl.github.io/blog/parsing-basics/

pub mod compiler;
pub mod lexer;
pub mod parser;

#[test]
fn funcs() {
    use parser::Parser;
    use compiler::Compiler;

    let input = r#"
        fn init() {
            let focus_count = 0;
            return;
        }

        fn process() {
            std::halt();
            return;
        }

        fn onTurn() {
            let rng = std::rand() % 5;
            if focus_count > 5 {
                rng = 4;
            }

            if rng < 2 {
                focus_count = focus_count + 1;
                battle::performMove(Target.AllEnemies, 0x301C); // Focus
                return;
            }

            battle::defineActorSubset(Target.Frontline, ActorProperty.IsAlive, 0, Selector.AnyAll);
            battle::defineActorSubset(Target.Predefined, ActorProperty.MagicDefense, 0, Selector.Lowest);
            battle::defineActorSubset(Target.Predefined, ActorProperty.Hp, 0, Selector.Lowest);
            battle::performMove(Target.Predefined, 0x3044); // Water
            return;
        }"#;
    let mut parser = Parser::new(input);
    let v = parser.file();
    let mut compiler = Compiler::new(v);
    let ops = compiler.parse_asm();
    let compiled = compiler.compile(ops);
    println!("{}", compiled);
}

#[test]
fn namespace() {
    use parser::Parser;

    let input = r#"fn main() { Battle::Common::getActorProperty(Target.Self, 15); }"#;
    let mut parser = Parser::new(input);
    let v = parser.file();
    assert_eq!(format!("{}", (v.iter().map(|item| format!("{:?}", item)).collect::<Vec<String>>()).join(", ")), "");
}

#[test]
fn ops_to_bytes() {
    let ops: Vec<compiler::ops::OpCode> = vec![];

    let mut bytes = vec![];
    for op in ops {
        bytes.extend(op.instruction());
    }

    let str_bytes: Vec<String> = bytes.iter().map(|n| format!("{:02X}", n)).collect();

    assert_eq!(format!("{}", str_bytes.join(" ")), "");
}