use std::{io::{self, Write}, process::Command};

use crate::{
    lexer::{Token, TokenKind},
    T,
};

use super::{ast, Parser};

trait Operator {
    /// Prefix operators bind their operand to the right.
    fn prefix_binding_power(&self) -> ((), u8);

    /// Infix operators bind two operands, lhs and rhs.
    fn infix_binding_power(&self) -> Option<(u8, u8)>;

    /// Postfix operators bind their operand to the left.
    fn postfix_binding_power(&self) -> Option<(u8, ())>;
}

impl Operator for TokenKind {
    fn prefix_binding_power(&self) -> ((), u8) {
        match self {
            T![+] | T![-] | T![!] | T![~] => ((), 128),
            _ => unreachable!("Not a prefix operator: {:?}", self),
        }
    }

    fn infix_binding_power(&self) -> Option<(u8, u8)> {
        let result = match self {
            T![||] => (1, 2),
            T![&&] => (3, 4),
            T![==] | T![!=] => (5, 6),
            T![<] | T![>] | T![u<] | T![u>] | T![<=] | T![>=] | T![u<=] | T![u>=] => (7, 8),
            T![<<] | T![>>] => (9, 10),
            T![+] | T![-] => (11, 12),
            T![*] | T![/] => (13, 14),
            T![%] => (15, 16),
            T![&] | T![|] | T![^] => (17, 18),
            T![b!] | T![b!!] => (19, 20),
            T![.] => (90, 91),
            _ => return None,
        };

        Some(result)
    }

    fn postfix_binding_power(&self) -> Option<(u8, ())> {
        return None; // there are no postfix operators so far...

        let result = match self {
            _ => return None,
        };

        Some(result)
    }
}

impl<'input, I> Parser<'input, I>
where
    I: Iterator<Item = Token>,
{
    pub fn parse_expression(&mut self, binding_power: u8, is_direct: bool) -> ast::Expr {
        let mut lhs = match self.peek() {
            lit @ T![int] | lit @ T![float] | lit @ T![string] => {
                let literal_text = {
                    // if `peek` is not `T![EOF]`, then there must be a next token
                    let literal_token = self.next().unwrap();
                    self.text(literal_token)
                };

                let lit = match lit {
                    T![int] => {
                        let value = if literal_text.starts_with("0x") {
                            let literal_text: String = literal_text.chars().skip(2).collect();

                            u32::from_str_radix(&literal_text, 16).unwrap_or_else(|_| {
                                panic!("invalid hex integer literal: `{}`", literal_text)
                            })
                        } else {
                            literal_text.parse().unwrap_or_else(|_| {
                                panic!("invalid integer literal: `{}`", literal_text)
                            })
                        };

                        ast::Lit::Int(value)
                    }
                    T![float] => ast::Lit::Float(literal_text.parse().unwrap_or_else(|_| {
                        panic!("invalid floating point literal: `{}`", literal_text)
                    })),
                    T![string] => ast::Lit::Str(
                        // trim quotation marks
                        literal_text[1..(literal_text.len() - 1)].to_string(),
                    ),
                    _ => unreachable!(),
                };

                ast::Expr::Literal(lit)
            }
            T![ident] => {
                let mut name = {
                    let ident_token = self.next().unwrap();
                    self.text(ident_token).to_string() // we need a copy
                };

                match self.peek() {
                    T!['('] | T![::] => { // function call
                        let mut namespace = None;
                        if self.at(T![::]) {
                            self.consume(T![::]);
                            namespace = Some(name.clone());
                            name = {
                                let name_token = self.next().expect("Tried to parse function name, but there were no more tokens");
                                assert_eq!(
                                    name_token.kind,
                                    T![ident],
                                    "Expected identifier as function name, but found `{}`",
                                    name_token.kind
                                );

                                self.text(name_token).to_string()
                            }
                        }
    
                        //  function call
                        let mut args = Vec::new();
                        self.consume(T!['(']);
                        while !self.at(T![')']) {
                            let arg = self.parse_expression(0, false);
                            args.push(arg);
                            if self.at(T![,]) {
                                self.consume(T![,]);
                            }
                        }
                        self.consume(T![')']);
    
                        ast::Expr::FnCall {
                            namespace,
                            name,
                            args,
                        }
                    },
                    T![.] => { // Since structs aren't supported, this is an enum access
                        let enum_name = name;
                        self.consume(T![.]);
                        let variant_name = {
                            let variant_token = self.next().expect("Expected variant name after enum access, but there were no other tokens");
                            assert_eq!(variant_token.kind, T![ident], "Expected identifier as variant name, but found `{}`", variant_token.kind);
                            self.text(variant_token).to_string()
                        };

                        ast::Expr::EnumAccess {
                            enum_name,
                            variant_name,
                        }
                    }
                    _ => ast::Expr::Ident(name)
                }
            }
            T!['('] => {
                // There is no AST node for grouped expressions
                // Parentheses just influence the tree structure
                self.consume(T!['(']);
                let expr = self.parse_expression(0, false);
                self.consume(T![')']);
                expr
            }
            op @ T![+] | op @ T![-] | op @ T![!] | op @ T![~] => {
                self.consume(op);
                let ((), right_binding_power) = op.prefix_binding_power();
                let expr = self.parse_expression(right_binding_power, false);
                ast::Expr::PrefixOp {
                    op,
                    expr: Box::new(expr),
                }
            }
            kind => {
                panic!("Unknown start of expression: `{}` near {:?}", kind, self.tokens.peek_amount(4));
            }
        };

        if is_direct {
            return lhs;
        }

        loop {
            let op = match self.peek() {
                op @ T![||]
                | op @ T![&&]
                | op @ T![|]
                | op @ T![^]
                | op @ T![&]
                | op @ T![==]
                | op @ T![!=]
                | op @ T![u>]
                | op @ T![u<]
                | op @ T![>]
                | op @ T![<]
                | op @ T![u>=]
                | op @ T![u<=]
                | op @ T![>=]
                | op @ T![<=]
                | op @ T![b!]
                | op @ T![b!!]
                | op @ T![<<]
                | op @ T![>>]
                | op @ T![+]
                | op @ T![-]
                | op @ T![*]
                | op @ T![/]
                | op @ T![%]
                | op @ T![!]
                | op @ T![~]
                | op @ T![.] => op,
                T![EOF] => break,
                T!['{'] | T!['('] | T![')'] | T!['}'] | T![,] | T![;] | T![=] | T![->] | T![=>] | T![::] => {
                    break
                }
                kind => panic!("Unknown operator: `{}` near {:?}", kind, self.tokens.peek_amount(5)),
            };

            if let Some((left_binding_power, ())) = op.postfix_binding_power() {
                if left_binding_power < binding_power {
                    // previous operator has higher binding power than new one, so we move on
                    break;
                }

                self.consume(op);
                // no recursive call here cause we have already parsed our `lhs` operand
                lhs = ast::Expr::PostfixOp {
                    op,
                    expr: Box::new(lhs),
                };

                continue; // parsed an operator so we loop again
            }

            if let Some((left_binding_power, ())) = op.postfix_binding_power() {
                if left_binding_power < binding_power {
                    break; // previous operator has higher binding power than new one, so we move on
                }

                self.consume(op);
                // no recursive call here cause we have already parsed our `lhs` operand
                lhs = ast::Expr::PostfixOp {
                    op,
                    expr: Box::new(lhs),
                };

                continue; // parsed an operator so we loop again
            }

            if let Some((left_binding_power, right_binding_power)) = op.infix_binding_power() {
                if left_binding_power < binding_power {
                    // previous operator has higher binding power than new one, so we move on
                    break;
                }

                self.consume(op);
                let rhs = self.parse_expression(right_binding_power, false);
                lhs = ast::Expr::InfixOp {
                    op,
                    lhs: Box::new(lhs),
                    rhs: Box::new(rhs),
                };

                continue; // parsed an operator so we loop again
            }

            break; // not an operator so we move on
        }

        lhs
    }
}
