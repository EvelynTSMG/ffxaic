use maechen::script::Func;

use crate::{compiler::{ops::OpCode, Compiler, self}, lexer, T};

use super::ast;



impl ast::Expr {
    pub fn ops(self: &ast::Expr, compiler: &mut Compiler, alone: bool) -> Vec<OpCode> {
        let mut ops = vec![];

        match self {
            ast::Expr::FnCall { .. } => ops.push(self.ops_fncall(compiler, alone)),
            ast::Expr::Literal(_) => ops.push(self.ops_lit(compiler)),
            ast::Expr::Ident(var_name) => {
                ops.push(OpCode::PUSHV(compiler.var(&var_name)));
            },
            ast::Expr::EnumAccess { enum_name, variant_name } => {
                let value = maechen::get_enum(enum_name, variant_name);
                ops.push(OpCode::PUSHII(value as u16));
            },
            ast::Expr::PrefixOp { op, expr } => {
                ops.extend(ast::Expr::ops_prefix_op(compiler, *op, *expr.clone()));
            },
            ast::Expr::InfixOp { op, lhs, rhs } => {
                ops.extend(ast::Expr::ops_infix_op(compiler, *op, *lhs.clone(), *rhs.clone()));
            },
            ast::Expr::PostfixOp { op, expr } => {
                ops.extend(ast::Expr::ops_postfix_op(compiler, *op, *expr.clone()));
            },
        }

        ops
    }

    fn ops_lit(self: &ast::Expr, compiler: &mut Compiler) -> OpCode {
        if let ast::Expr::Literal(lit) = self {
            match lit {
                ast::Lit::Int(val) => {
                    if val <= &(u16::MAX as u32) {
                        return OpCode::PUSHII(*val as u16)
                    }

                    if !compiler.int_constants.contains(&val) {
                        compiler.int_constants.push(*val);
                    }
                    OpCode::PUSHI(compiler.int(*val))
                },
                ast::Lit::Float(val) => {
                    if !compiler.float_constants.contains(&(val)) {
                        compiler.float_constants.push(*val);
                    }
                    OpCode::PUSHF(compiler.float(*val))
                },
                ast::Lit::Str(_) => {
                    panic!("String literals are not supported");
                }
            }
        } else {
            panic!("Tried to parse `{:?}` as literal", self);
        }
    }

    fn ops_fncall(self: &ast::Expr, compiler: &mut Compiler, alone: bool) -> OpCode {
        if let ast::Expr::FnCall { namespace, name, args } = self {
            let mut funcs = Func::get_func(namespace.as_deref(), name);

            // This is jank
            funcs.retain(|f| f.args.len() == args.len());
            let func = &funcs[0];

            let addr = func.addr;

            let mut arguments = vec![];
            for arg in args {
                arguments.extend(arg.ops(compiler, false).iter().map(|n| Box::new(n.clone())));
            }

            match alone {
                true => return OpCode::CALLPOPA {
                    addr,
                    args: arguments,
                },
                false => return OpCode::CALL {
                    addr,
                    args: arguments,
                },
            }
        } else {
            panic!("Tried to parse `{:?}` as function", self);
        }
    }

    fn ops_prefix_op(compiler: &mut Compiler,op: lexer::TokenKind, operand: ast::Expr) -> Vec<OpCode> {
        let mut ops = vec![];
        let operand = operand.ops(compiler, false);

        match op {
            T![+] => ops.extend(operand),
            T![-] => {
                ops.push(OpCode::OPUMINUS {
                    operand: Box::new(operand),
                });
            },
            T![!] => {
                ops.push(OpCode::OPNOT {
                    operand: Box::new(operand),
                });
            },
            T![~] => {
                ops.push(OpCode::OPBNOT {
                    operand: Box::new(operand),
                });
            },
            _ => unreachable!(),
        }

        ops
    }

    fn ops_infix_op(compiler: &mut Compiler, op: lexer::TokenKind, lhs: ast::Expr, rhs: ast::Expr) -> Vec<OpCode> {
        let mut ops = vec![];
        let lhs = Box::new(lhs.ops(compiler, false));
        let rhs = Box::new(rhs.ops(compiler, false));

        match op {
            T![||] => ops.push(OpCode::OPLOR { lhs, rhs }),
            T![&&] => ops.push(OpCode::OPLAND { lhs, rhs }),
            T![==] => ops.push(OpCode::OPEQ { lhs, rhs }),
            T![!=] => ops.push(OpCode::OPNE { lhs, rhs }),
            T![<] => ops.push(OpCode::OPLS { lhs, rhs }),
            T![>] => ops.push(OpCode::OPGT { lhs, rhs }),
            T![u<] => ops.push(OpCode::OPLSU { lhs, rhs }),
            T![u>] => ops.push(OpCode::OPGTU { lhs, rhs }),
            T![<=] => ops.push(OpCode::OPLSE { lhs, rhs }),
            T![>=] => ops.push(OpCode::OPGTE { lhs, rhs }),
            T![u<=] => ops.push(OpCode::OPLSEU { lhs, rhs }),
            T![u>=] => ops.push(OpCode::OPGTEU { lhs, rhs }),
            T![<<] => ops.push(OpCode::OPSLL { lhs, rhs }),
            T![>>] => ops.push(OpCode::OPSRL { lhs, rhs }),
            T![+] => ops.push(OpCode::OPADD { lhs, rhs }),
            T![-] => ops.push(OpCode::OPSUB { lhs, rhs }),
            T![*] => ops.push(OpCode::OPMUL { lhs, rhs }),
            T![/] => ops.push(OpCode::OPDIV { lhs, rhs }),
            T![%] => ops.push(OpCode::OPMOD { lhs, rhs }),
            T![&] => ops.push(OpCode::OPAND { lhs, rhs }),
            T![|] => ops.push(OpCode::OPOR { lhs, rhs }),
            T![^] => ops.push(OpCode::OPEOR { lhs, rhs }),
            T![b!] => ops.push(OpCode::OPBOFF { lhs, rhs }),
            T![b!!] => ops.push(OpCode::OPBON { lhs, rhs }),
            _ => unreachable!()
        }

        ops
    }

    fn ops_postfix_op(compiler: &mut Compiler, op: lexer::TokenKind, operand: ast::Expr) -> Vec<OpCode> {
        let mut ops = vec![];

        // There are none
        panic!("There are no prefix operators but the parser detected some");

        ops
    }
}

impl ast::Item {
    pub fn ops(self: &ast::Item, compiler: &mut Compiler) -> Vec<OpCode> {
        let mut ops = vec![];

        match self {
            ast::Item::Function { .. } => ops.extend(self.ops_func(compiler)),
            _ => todo!()
        }
        
        ops
    }

    fn ops_func(self: &ast::Item, compiler: &mut Compiler) -> Vec<OpCode> {
        let mut ops = vec![];

        let ast::Item::Function { name, parameters, body } = self;

        if Compiler::BASE_FUNCS.contains(&name.as_str()) {
            // We don't have to care about parameters

            // Tag for entry point of function
            ops.push(OpCode::TAG(compiler.next_entry_point()));

            for stmt in body {
                ops.extend(stmt.ops(compiler));
            }
        }

        ops
    }
}

impl ast::Stmt {
    pub fn ops(self: &ast::Stmt, compiler: &mut Compiler) -> Vec<OpCode> {
        let mut ops = vec![];

        match self {
            ast::Stmt::Block { stmts } => {
                for stmt in stmts {
                    ops.extend(stmt.ops(compiler));
                }
            },
            ast::Stmt::RetStmt => {
                ops.push(OpCode::RET);
            },
            ast::Stmt::DirectExpr { expr } => {
                ops.extend(expr.ops(compiler, true));
            },
            ast::Stmt::Assignment { var_name, value } => {
                ops.extend(value.ops(compiler, false));
                ops.push(OpCode::POPV(compiler.var(var_name)));
            },
            ast::Stmt::Let { var_name, value } => {
                compiler.new_var(var_name);

                ops.extend(value.ops(compiler, false));
                ops.push(OpCode::POPV(compiler.var(var_name)));
            },
            ast::Stmt::IfStmt { condition, body, else_stmt } => {
                ops.extend(condition.ops(compiler, false));

                let jump_id = compiler.next_jump();
                ops.push(OpCode::POPXNCJMP(jump_id));

                for stmt in body {
                    ops.extend(stmt.ops(compiler));
                }

                if let Some(stmt) = else_stmt {
                    let else_jump_id = compiler.next_jump();
                    
                    // Get out of if statement after true condition
                    ops.push(OpCode::JMP(else_jump_id)); 

                    ops.push(OpCode::LABEL(jump_id)); // } else {

                    ops.extend(stmt.ops(compiler)); // ...

                    ops.push(OpCode::LABEL(else_jump_id)); // }
                } else {
                    ops.push(OpCode::LABEL(jump_id));
                }
            },
            ast::Stmt::MatchStmt { var_name, cases } => {
                ops.push(OpCode::PUSHV(compiler.var(var_name)));
                ops.push(OpCode::POPY);
                // the Y register is now the matching variable
                let mut case_jump_ids = vec![];
                for case in cases.iter() {
                    case_jump_ids.push(compiler.next_jump());
                    let case_jump_id = *case_jump_ids.last().unwrap();
                    if let Some(expr) = &case.0.condition {
                        ops.push(OpCode::OPLAND {
                            lhs: Box::new(vec![OpCode::OPEQ {
                                lhs: Box::new(vec![OpCode::PUSHY]),
                                rhs: Box::new(case.0.pattern.ops(compiler, false)),
                            }]),
                            rhs: Box::new(expr.ops(compiler, false)),
                        });
                    }
                    ops.push(OpCode::POPXCJMP(case_jump_id));
                }
                let after_jump_id = compiler.next_jump();
                ops.push(OpCode::JMP(after_jump_id));

                for i in 0..cases.len() {
                    let case = &cases[i];
                    ops.push(OpCode::LABEL(case_jump_ids[i]));
                    ops.extend(case.1.ops(compiler));
                }
                
                ops.push(OpCode::LABEL(after_jump_id));
            }
        }

        ops
    }
}