use std::fmt;

use crate::lexer::TokenKind;

#[derive(Debug, Clone, PartialEq)]
pub enum Expr {
    Literal(Lit),
    Ident(String),
    FnCall {
        namespace: Option<String>,
        name: String,
        args: Vec<Expr>,
    },
    EnumAccess {
        enum_name: String,
        variant_name: String,
    },
    PrefixOp {
        op: TokenKind,
        expr: Box<Expr>,
    },
    InfixOp {
        op: TokenKind,
        lhs: Box<Expr>,
        rhs: Box<Expr>,
    },
    PostfixOp {
        op: TokenKind,
        expr: Box<Expr>,
    },
}

#[derive(Debug, Clone, PartialEq)]
pub enum Lit {
    Int(u32),
    Float(f32),
    Str(String),
}

impl fmt::Display for Expr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Expr::Literal(lit) => write!(f, "{}", lit),
            Expr::Ident(name) => write!(f, "{}", name),
            Expr::FnCall { namespace, name, args } => {
                if let Some(namespace) = namespace {
                    write!(f, "{}::{}(", namespace, name)?;
                } else {
                    write!(f, "{}(", name)?;
                }
                for arg in &args[..(args.len() - 1)] {
                    write!(f, "{},", arg)?;
                }
                write!(f, "{})", args[args.len() - 1])
            }
            Expr::EnumAccess { enum_name, variant_name } => {
                write!(f, "{}.{}", enum_name, variant_name)
            }
            Expr::PrefixOp { op, expr } => write!(f, "({}{})", op, expr),
            Expr::InfixOp { op, lhs, rhs } => write!(f, "({} {} {})", lhs, op, rhs),
            Expr::PostfixOp { op, expr } => write!(f, "({}{})", expr, op),
        }
    }
}

impl fmt::Display for Lit {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Lit::Int(i) => write!(f, "{}", i),
            Lit::Float(fl) => write!(f, "{}", fl),
            Lit::Str(s) => write!(f, "\"{}\"", s),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct MatchCondition {
    pub pattern: Box<Expr>,
    pub condition: Option<Box<Expr>>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Stmt {
    DirectExpr {
        expr: Expr,
    },
    Let {
        var_name: String,
        value: Box<Expr>,
    },
    Assignment {
        var_name: String,
        value: Box<Expr>,
    },
    IfStmt {
        condition: Box<Expr>,
        body: Vec<Stmt>,
        else_stmt: Option<Box<Stmt>>,
    },
    MatchStmt {
        var_name: String,
        cases: Vec<(Box<MatchCondition>, Box<Stmt>)>,
    },
    Block {
        stmts: Vec<Stmt>,
    },
    RetStmt
}

#[derive(Debug, Clone, PartialEq)]
pub enum Item {
    Function {
        name: String,
        parameters: Vec<(String, Type)>,
        body: Vec<Stmt>,
    },
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Type {
    pub name: String,
    pub generics: Vec<Type>,
}
