use std::io::{self, Write};

use crate::{lexer::Token, T};
use super::{ast, Parser};

impl<'input, I> Parser<'input, I>
where
    I: Iterator<Item = Token>,
{
    pub fn statement(&mut self) -> ast::Stmt {
        match self.peek() {
            T![let] => {
                self.consume(T![let]);
                let ident = self.next().expect("Expected identifier after `let`");
                assert_eq!(
                    ident.kind,
                    T![ident],
                    "Expected identifier after `let`, but found `{}`",
                    ident.kind
                );
                let name = self.text(ident).to_string();

                self.consume(T![=]);
                let value = self.expression(false);
                self.consume(T![;]);

                ast::Stmt::Let {
                    var_name: name,
                    value: Box::new(value),
                }
            },
            T![ret] => {
                self.consume(T![ret]);
                self.consume(T![;]);
                ast::Stmt::RetStmt
            },
            T![ident] => {
                self.tokens.advance_cursor();
                if self.peek() == T![=] {
                    self.tokens.reset_cursor();
                    let name = self.next().unwrap();
                    let name = self.text(name).to_string();

                    self.consume(T![=]);
                    let value = self.expression(false);
                    self.consume(T![;]);
    
                    ast::Stmt::Assignment {
                        var_name: name,
                        value: Box::new(value),
                    }
                } else {
                    self.tokens.reset_cursor();

                    let expr = ast::Stmt::DirectExpr {
                        expr: self.expression(true)
                    };
                    self.consume(T![;]);
                    return expr;
                }
            },
            T![if] => {
                self.consume(T![if]);
                let condition = self.expression(false);

                assert!(self.at(T!['{']), "Expected a block after `if` statement");
                let body = self.statement();
                let body = match body {
                    ast::Stmt::Block { stmts } => stmts,
                    _ => unreachable!(),
                };

                let else_stmt = if self.at(T![else]) {
                    self.consume(T![else]);
                    assert!(
                        self.at(T![if]) || self.at(T!['{']),
                        "Expected a block or an `if` after `else` statement"
                    );
                    Some(Box::new(self.statement()))
                } else {
                    None
                };

                ast::Stmt::IfStmt {
                    condition: Box::new(condition),
                    body,
                    else_stmt,
                }
            },
            T![match] => {
                self.consume(T![match]);

                let ident = self.next().expect("Expected identifier after `match`");
                assert_eq!(
                    ident.kind,
                    T![ident],
                    "Expected identifier after `match`, but found `{}`",
                    ident.kind
                );
                let var_name = self.text(ident).to_string();

                assert!(
                    self.at(T!['{']),
                    "Expected match body after `match` statement"
                );
                self.consume(T!['{']);
                let mut cases: Vec<(Box<ast::MatchCondition>, Box<ast::Stmt>)> = vec![];
                loop {
                    if self.at(T!['}']) {
                        self.consume(T!['}']);
                        break;
                    } else {
                        let pattern = self.expression(false);
                        let pattern = Box::new(pattern);

                        let condition = if self.at(T![if]) {
                            self.consume(T![if]);
                            Some(Box::new(self.expression(false)))
                        } else {
                            None
                        };

                        let match_condition = ast::MatchCondition {
                            pattern,
                            condition,
                        };

                        assert!(
                            self.at(T![=>]),
                            "Expected a `=>` token after `match` condition"
                        );
                        self.consume(T![=>]);

                        assert!(self.at(T!['{']), "Expected a block after `match` case");
                        let body = self.statement();

                        if !self.at(T!['}']) {
                            assert!(self.at(T![,]), "Expected `,` after `match` case block");
                            self.consume(T![,]);
                        }

                        cases.push((Box::new(match_condition), Box::new(body)));
                    }
                }

                ast::Stmt::MatchStmt { var_name, cases }
            },
            T!['{'] => {
                self.consume(T!['{']);
                let mut stmts = Vec::new();

                while !self.at(T!['}']) {
                    let stmt = self.statement();
                    stmts.push(stmt);
                }
                self.consume(T!['}']);

                ast::Stmt::Block { stmts }
            },
            _ => {
                let expr = ast::Stmt::DirectExpr {
                    expr: self.expression(true),
                };
                self.consume(T![;]);
                expr
            },
        }
    }

    pub fn type_(&mut self) -> ast::Type {
        let ident = self
            .next()
            .expect("Tried to parse type, but there were no more tokens");
        assert_eq!(
            ident.kind,
            T![ident],
            "Expected identifier at start of type, but found `{}`",
            ident.kind
        );

        let name = self.text(ident).to_string();

        let mut generics = vec![];
        if self.at(T![<]) {
            self.consume(T![<]);

            while !self.at(T![>]) {
                // Generic parameters are also types
                let generic = self.type_();
                generics.push(generic);
                if self.at(T![,]) {
                    self.consume(T![,]);
                }
            }
            self.consume(T![>]);
        }

        ast::Type { name, generics }
    }

    pub fn item(&mut self) -> ast::Item {
        match self.peek() {
            T![fn] => {
                self.consume(T![fn]);
                let mut parameters = vec![];

                let ident = self
                    .next()
                    .expect("Tried to parse function name, but there were no more tokens");
                assert_eq!(
                    ident.kind,
                    T![ident],
                    "Expected identifier as function name, but found `{}`",
                    ident.kind
                );
                let name = self.text(ident).to_string();

                self.consume(T!['(']);

                while !self.at(T![')']) {
                    let parameter_ident = self
                        .next()
                        .expect("Tried to parse function paremeter, but there were no more tokens");
                    assert_eq!(
                        parameter_ident.kind,
                        T![ident],
                        "Expected identifier as function parameter name, but found `{}`",
                        parameter_ident.kind
                    );
                    let parameter_name = self.text(parameter_ident).to_string();
                    self.consume(T![:]);

                    let parameter_type = self.type_();
                    parameters.push((parameter_name, parameter_type));

                    if self.at(T![,]) {
                        self.consume(T![,]);
                    }
                }
                self.consume(T![')']);

                assert!(self.at(T!['{']), "Expected a block after function header");
                let body = match self.statement() {
                    ast::Stmt::Block { stmts } => stmts,
                    _ => unreachable!(),
                };

                ast::Item::Function {
                    name,
                    parameters,
                    body,
                }
            }
            kind => panic!("Unknown start of item: `{}`", kind),
        }
    }
}
