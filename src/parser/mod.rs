use std::iter::Peekable;
use peekmore::{PeekMore, PeekMoreIterator};

use crate::{
    lexer::{Lexer, Token, TokenKind},
    T,
};

pub mod ast;
pub mod expressions;
pub mod hierarchy;
pub mod ops;

pub struct TokenIter<'input> {
    lexer: Lexer<'input>,
}

impl<'input> TokenIter<'input> {
    pub fn new(input: &'input str) -> Self {
        Self {
            lexer: Lexer::new(input),
        }
    }
}

impl<'input> Iterator for TokenIter<'input> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let next_token = self.lexer.next()?;
            if !matches!(next_token.kind, T![ws] | T![comment]) {
                return Some(next_token);
            }
        }
    }
}

pub struct Parser<'input, I>
where
    I: Iterator<Item = Token>,
{
    input: &'input str,
    tokens: PeekMoreIterator<I>,
}

impl<'input> Parser<'input, TokenIter<'input>> {
    pub fn new(input: &'input str) -> Parser<'input, TokenIter<'input>> {
        Parser {
            input,
            tokens: TokenIter::new(input).peekmore(),
        }
    }
}

impl<'input, I> Parser<'input, I>
where
    I: Iterator<Item = Token>,
{
    /// Get the source text of a token.
    pub fn text(&self, token: Token) -> &'input str {
        token.text(self.input)
    }

    /// Look ahead one token and see what kind of token it is
    pub(crate) fn peek(&mut self) -> TokenKind {
        self.tokens
            .peek()
            .map(|token| token.kind)
            .unwrap_or(T![EOF])
    }

    /// Check if the next token is some `kind` of token
    pub(crate) fn at(&mut self, kind: TokenKind) -> bool {
        self.peek() == kind
    }

    /// Get the next token
    pub(crate) fn next(&mut self) -> Option<Token> {
        self.tokens.next()
    }

    /// Move forward one token in the input and check
    /// that we pass the kind of token we expect.
    pub(crate) fn consume(&mut self, expected: TokenKind) {
        let token = self.next().unwrap_or_else(|| {
            panic!(
                "Expected to consume `{}`, but there was no next token",
                expected
            )
        });

        assert_eq!(
            token.kind, expected,
            "Expected to consume `{}`, but found `{}` near {}",
            expected, token.kind, self.text(Token { kind: T![error], span: crate::lexer::Span { start: 0, end: token.span.end } })
        );
    }

    pub fn expression(&mut self, is_direct: bool) -> ast::Expr {
        self.parse_expression(0, is_direct)
    }

    pub fn file(&mut self) -> Vec<ast::Item> {
        let mut items = vec![];

        while !self.at(T![EOF]) {
            let item = self.item();
            items.push(item);
        }

        items
    }
}
