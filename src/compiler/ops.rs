pub enum ArgType {
    Void = 0,
    Int = 1,
    Float = 2,
}

pub struct Oper {
    in_type: ArgType,
    out_type: ArgType,
    instruction_size: u8,
    op_type: u8,
}

impl Default for Oper {
    fn default() -> Self {
        Oper {
            in_type: ArgType::Void,
            out_type: ArgType::Void,
            instruction_size: 1,
            op_type: 0,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum OpCode {
    NOP,
    OPLOR {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPLAND {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPOR {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPEOR {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPAND {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPEQ {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPNE {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPGTU {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPLSU {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPGT {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPLS {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPGTEU {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPLSEU {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPGTE {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPLSE {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPBON {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPBOFF {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPSLL {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPSRL {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPADD {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPSUB {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPMUL {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPDIV {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPMOD {
        lhs: Box<Vec<OpCode>>,
        rhs: Box<Vec<OpCode>>,
    },
    OPNOT {
        operand: Box<Vec<OpCode>>,
    },
    OPUMINUS {
        operand: Box<Vec<OpCode>>,
    },
    OPFIXADRS,
    OPBNOT {
        operand: Box<Vec<OpCode>>,
    },
    /// Used for jumps
    LABEL(u16),
    /// Used for entry points
    TAG(u16),
    PUSHV(u16),
    POPV(u16),
    POPVL(u16),
    PUSHAR {
        id: u16,
        idx: Box<OpCode>,
    },
    POPAR {
        id: u16,
        idx: Box<OpCode>,
    },
    POPARL {
        id: u16,
        idx: Box<OpCode>,
    },
    POPA,
    PUSHA,
    PUSHARP {
        id: u16,
        idx: Box<OpCode>,
    },
    PUSHX,
    PUSHY,
    POPX,
    REPUSH,
    POPY,
    PUSHI(u16),
    PUSHII(u16),
    PUSHF(u16),
    JMP(u16),
    CJMP(u16),
    NCJMP(u16),
    JSR(u16),
    RTS,
    CALL {
        addr: u16,
        args: Vec<Box<OpCode>>,
    },
    REQ,
    REQSW,
    REQEW,
    PREQ,
    PREQSW,
    PREQEW,
    RET,
    RETN,
    RETT,
    RETTN,
    HALT,
    PUSHN,
    PUSHT,
    PUSHVP(u16),
    PUSHFIX(u16),
    FREQ,
    TREQ,
    BREQ,
    BFREQ,
    BTREQ,
    FREQSW,
    TREQSW,
    BREQSW,
    BFREQSW,
    BTREQSW,
    FREQEW,
    TREQEW,
    BREQEW,
    BFREQEW,
    BTREQEW,
    DRET,
    POPXJMP(u16),
    POPXCJMP(u16),
    POPXNCJMP(u16),
    CALLPOPA {
        addr: u16,
        args: Vec<Box<OpCode>>,
    },
    POPI0,
    POPI1,
    POPI2,
    POPI3,
    POPF0,
    POPF1,
    POPF2,
    POPF3,
    POPF4,
    POPF5,
    POPF6,
    POPF7,
    POPF8,
    POPF9,
    PUSHI0,
    PUSHI1,
    PUSHI2,
    PUSHI3,
    PUSHF0,
    PUSHF1,
    PUSHF2,
    PUSHF3,
    PUSHF4,
    PUSHF5,
    PUSHF6,
    PUSHF7,
    PUSHF8,
    PUSHF9,
    PUSHAINTER(u16),
    SYSTEM(u16),
    REQWAIT,
    PREQWAIT,
    REQCHG,
    ACTREQ,
}

impl OpCode {
    pub fn oper(&self) -> Oper {
        match self {
            OpCode::NOP => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 0,
                op_type: 0,
            },
            OpCode::OPLOR { .. }
            | OpCode::OPLAND { .. }
            | OpCode::OPOR { .. }
            | OpCode::OPEOR { .. }
            | OpCode::OPAND { .. } => Oper {
                in_type: ArgType::Int,
                out_type: ArgType::Int,
                instruction_size: 1,
                op_type: 0,
            },
            OpCode::OPEQ { .. }
            | OpCode::OPNE { .. }
            | OpCode::OPGTU { .. }
            | OpCode::OPLSU { .. }
            | OpCode::OPGT { .. }
            | OpCode::OPLS { .. }
            | OpCode::OPGTEU { .. }
            | OpCode::OPLSEU { .. }
            | OpCode::OPGTE { .. }
            | OpCode::OPLSE { .. } => Oper {
                in_type: ArgType::Float,
                out_type: ArgType::Int,
                instruction_size: 1,
                op_type: 0,
            },
            OpCode::OPBON { .. }
            | OpCode::OPBOFF { .. }
            | OpCode::OPSLL { .. }
            | OpCode::OPSRL { .. } => Oper {
                in_type: ArgType::Int,
                out_type: ArgType::Int,
                instruction_size: 1,
                op_type: 0,
            },
            OpCode::OPADD { .. }
            | OpCode::OPSUB { .. }
            | OpCode::OPMUL { .. }
            | OpCode::OPDIV { .. }
            | OpCode::OPMOD { .. } => Oper {
                in_type: ArgType::Float,
                out_type: ArgType::Float,
                instruction_size: 1,
                op_type: 0,
            },
            OpCode::OPNOT { .. } => Oper {
                in_type: ArgType::Int,
                out_type: ArgType::Int,
                instruction_size: 1,
                op_type: 0,
            },
            OpCode::OPUMINUS { .. } | OpCode::OPFIXADRS => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 1,
                op_type: 0,
            },
            OpCode::OPBNOT { .. } => Oper {
                in_type: ArgType::Int,
                out_type: ArgType::Int,
                instruction_size: 1,
                op_type: 0,
            },
            OpCode::LABEL(_) => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 0,
                op_type: 7,
            },
            OpCode::TAG(_) => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 0,
                op_type: 6,
            },
            OpCode::PUSHV { .. }
            | OpCode::POPV { .. }
            | OpCode::POPVL { .. }
            | OpCode::PUSHAR { .. }
            | OpCode::POPAR { .. }
            | OpCode::POPARL { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 3,
            },
            OpCode::POPA | OpCode::PUSHA => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 1,
                op_type: 0,
            },
            OpCode::PUSHARP { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 3,
            },
            OpCode::PUSHX | OpCode::PUSHY | OpCode::POPX | OpCode::POPY | OpCode::REPUSH => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 1,
                op_type: 0,
            },
            OpCode::PUSHI { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 2,
            },
            OpCode::PUSHII { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 1,
            },
            OpCode::PUSHF { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 5,
            },
            OpCode::JMP { .. } | OpCode::CJMP { .. } | OpCode::NCJMP { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 7,
            },
            OpCode::JSR { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 8,
            },
            OpCode::RTS => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 1,
                op_type: 0,
            },
            OpCode::CALL { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 4,
            },
            OpCode::REQ
            | OpCode::REQSW
            | OpCode::REQEW
            | OpCode::PREQ
            | OpCode::PREQSW
            | OpCode::PREQEW
            | OpCode::RET
            | OpCode::RETN
            | OpCode::RETT
            | OpCode::RETTN
            | OpCode::HALT => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 1,
                op_type: 0,
            },
            OpCode::PUSHN { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 8,
            },
            OpCode::PUSHT { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 6,
            },
            OpCode::PUSHVP { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 3,
            },
            OpCode::PUSHFIX { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 2,
            },
            OpCode::FREQ
            | OpCode::TREQ
            | OpCode::BREQ
            | OpCode::BFREQ
            | OpCode::BTREQ
            | OpCode::FREQSW
            | OpCode::TREQSW
            | OpCode::BREQSW
            | OpCode::BFREQSW
            | OpCode::BTREQSW
            | OpCode::FREQEW
            | OpCode::TREQEW
            | OpCode::BREQEW
            | OpCode::BFREQEW
            | OpCode::BTREQEW
            | OpCode::DRET => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 1,
                op_type: 0,
            },
            OpCode::POPXJMP { .. } | OpCode::POPXCJMP { .. } | OpCode::POPXNCJMP { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 7,
            },
            OpCode::CALLPOPA { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 4,
            },
            OpCode::POPI0
            | OpCode::POPI1
            | OpCode::POPI2
            | OpCode::POPI3
            | OpCode::POPF0
            | OpCode::POPF1
            | OpCode::POPF2
            | OpCode::POPF3
            | OpCode::POPF4
            | OpCode::POPF5
            | OpCode::POPF6
            | OpCode::POPF7
            | OpCode::POPF8
            | OpCode::POPF9
            | OpCode::PUSHI0
            | OpCode::PUSHI1
            | OpCode::PUSHI2
            | OpCode::PUSHI3
            | OpCode::PUSHF0
            | OpCode::PUSHF1
            | OpCode::PUSHF2
            | OpCode::PUSHF3
            | OpCode::PUSHF4
            | OpCode::PUSHF5
            | OpCode::PUSHF6
            | OpCode::PUSHF7
            | OpCode::PUSHF8
            | OpCode::PUSHF9 => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 1,
                op_type: 0,
            },
            OpCode::PUSHAINTER { .. } | OpCode::SYSTEM { .. } => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 3,
                op_type: 1,
            },
            OpCode::REQWAIT | OpCode::PREQWAIT | OpCode::REQCHG | OpCode::ACTREQ => Oper {
                in_type: ArgType::Void,
                out_type: ArgType::Void,
                instruction_size: 1,
                op_type: 0,
            },
        }
    }

    pub fn opcode(&self) -> u8 {
        match self {
            OpCode::NOP => 0x0,                 // No operation
            OpCode::OPLOR { .. } => 0x1,        // Logical OR; ||
            OpCode::OPLAND { .. } => 0x2,       // Logical AND; &&
            OpCode::OPOR { .. } => 0x3,         // Bitwise OR; |
            OpCode::OPEOR { .. } => 0x4,        // Bitwise XOR; ^
            OpCode::OPAND { .. } => 0x5,        // Bitwise AND; &
            OpCode::OPEQ { .. } => 0x6,         // Equality comparison; ==
            OpCode::OPNE { .. } => 0x7,         // Inequality comparison; !=
            OpCode::OPGTU { .. } => 0x8,        // Greater than (unsigned) comparison; u>
            OpCode::OPLSU { .. } => 0x9,        // Lesser than (unsigned) comparison; u<
            OpCode::OPGT { .. } => 0xA,         // Greater than comparison; >
            OpCode::OPLS { .. } => 0xB,         // Lesser than comparison; <
            OpCode::OPGTEU { .. } => 0xC,       // Greater than or equal (unsigned) comparison; u>=
            OpCode::OPLSEU { .. } => 0xD,       // Lesser than or equal (unsigned) comparison; u<=
            OpCode::OPGTE { .. } => 0xE,        // Greater than or equal comparison; >=
            OpCode::OPLSE { .. } => 0xF,        // Lesser than or equal comparison; <=
            OpCode::OPBON { .. } => 0x10,       // Bit on; b!
            OpCode::OPBOFF { .. } => 0x11,      // Bit off; b!!
            OpCode::OPSLL { .. } => 0x12,       // Shift left logical; <<
            OpCode::OPSRL { .. } => 0x13,       // Shift right logical; >>
            OpCode::OPADD { .. } => 0x14,       // Addition; +
            OpCode::OPSUB { .. } => 0x15,       // Subtraction; -
            OpCode::OPMUL { .. } => 0x16,       // Multiplication; *
            OpCode::OPDIV { .. } => 0x17,       // Division; /
            OpCode::OPMOD { .. } => 0x18,       // Modulo; %
            OpCode::OPNOT { .. } => 0x19,       // Logical NOT; !
            OpCode::OPUMINUS { .. } => 0x1A,    // Unary minus (negation); -
            OpCode::OPFIXADRS => 0x1B,          // ???
            OpCode::OPBNOT { .. } => 0x1C,      // Bitwise NOT; ~
            OpCode::LABEL(_) => 0x9D,           // Label, unused and results in softlock
            OpCode::TAG(_) => 0x9E,             // Tag, unused and results in softlock
            OpCode::PUSHV { .. } => 0x9F,       // Push to variable
            OpCode::POPV { .. } => 0xA0,        // Pop to variable
            OpCode::POPVL { .. } => 0xA1,       // Pop to variable (with cast)
            OpCode::PUSHAR { .. } => 0xA2,      // Push from array
            OpCode::POPAR { .. } => 0xA3,       // Pop to array
            OpCode::POPARL { .. } => 0xA4,      // Pop to array (with cast)
            OpCode::POPA => 0x25,               // Pop to register A
            OpCode::PUSHA => 0x26,              // Push from register A
            OpCode::PUSHARP { .. } => 0xA7,     // Push from array <P>???
            OpCode::PUSHX => 0x28,              // Push from register X
            OpCode::PUSHY => 0x29,              // Push from register Y
            OpCode::POPX => 0x2A,               // Pop to register X
            OpCode::REPUSH => 0x2B,             // Duplicate top value of stack
            OpCode::POPY => 0x2C,               // Pop to register Y
            OpCode::PUSHI { .. } => 0xAD,       // Push an integer
            OpCode::PUSHII { .. } => 0xAE,      // Push the integer following this operation
            OpCode::PUSHF { .. } => 0xAF,       // Push a float
            OpCode::JMP { .. } => 0xB0,         // Jump
            OpCode::CJMP { .. } => 0xB1,        // Jump if true
            OpCode::NCJMP { .. } => 0xB2,       // Jump if false
            OpCode::JSR { .. } => 0xB3,         // Jump to subroutine
            OpCode::RTS => 0x34,                // ???
            OpCode::CALL { .. } => 0xB5,        // Function call
            OpCode::REQ => 0x36,                // ???
            OpCode::REQSW => 0x37,              // ???
            OpCode::REQEW => 0x38,              // ???
            OpCode::PREQ => 0x39,               // ???
            OpCode::PREQSW => 0x3A,             // ???
            OpCode::PREQEW => 0x3B,             // ???
            OpCode::RET => 0x3C,                // Return to previous point
            OpCode::RETN => 0x3D,               // ???
            OpCode::RETT => 0x3E,               // ???
            OpCode::RETTN => 0x3F,              // ???
            OpCode::HALT => 0x40,               // ???
            OpCode::PUSHN => 0xC1,              // Push <N>???
            OpCode::PUSHT => 0xC2,              // Push <T>???
            OpCode::PUSHVP { .. } => 0xC3,      // Push from variable <P>???
            OpCode::PUSHFIX { .. } => 0xC4,     // ???
            OpCode::FREQ => 0x45,               // ???
            OpCode::TREQ => 0x46,               // ???
            OpCode::BREQ => 0x47,               // ???
            OpCode::BFREQ => 0x48,              // ???
            OpCode::BTREQ => 0x49,              // ???
            OpCode::FREQSW => 0x4A,             // ???
            OpCode::TREQSW => 0x4B,             // ???
            OpCode::BREQSW => 0x4C,             // ???
            OpCode::BFREQSW => 0x4D,            // ???
            OpCode::BTREQSW => 0x4E,            // ???
            OpCode::FREQEW => 0x4F,             // ???
            OpCode::TREQEW => 0x50,             // ???
            OpCode::BREQEW => 0x51,             // ???
            OpCode::BFREQEW => 0x52,            // ???
            OpCode::BTREQEW => 0x53,            // ???
            OpCode::DRET => 0x54,               // ???
            OpCode::POPXJMP { .. } =>  0xD5,    // Jump and pop to register X
            OpCode::POPXCJMP { .. } => 0xD6,    // Jump if true and pop to register X
            OpCode::POPXNCJMP { .. } => 0xD7,   // Jump if false and pop to register X
            OpCode::CALLPOPA { .. } => 0xD8,    // Function call and pop to register A
            OpCode::POPI0 => 0x59,              // Pop to 0th integer register
            OpCode::POPI1 => 0x5A,              // Pop to 1st integer register
            OpCode::POPI2 => 0x5B,              // Pop to 2nd integer register
            OpCode::POPI3 => 0x5C,              // Pop to 3rd integer register
            OpCode::POPF0 => 0x5D,              // Pop to 0th float register
            OpCode::POPF1 => 0x5E,              // Pop to 1st float register
            OpCode::POPF2 => 0x5F,              // Pop to 2nd float register
            OpCode::POPF3 => 0x60,              // Pop to 3rd float register
            OpCode::POPF4 => 0x61,              // Pop to 4th float register
            OpCode::POPF5 => 0x62,              // Pop to 5th float register
            OpCode::POPF6 => 0x63,              // Pop to 6th float register
            OpCode::POPF7 => 0x64,              // Pop to 7th float register
            OpCode::POPF8 => 0x65,              // Pop to 8th float register
            OpCode::POPF9 => 0x66,              // Pop to 9th float register
            OpCode::PUSHI0 => 0x67,             // Push from 0th integer register
            OpCode::PUSHI1 => 0x68,             // Push from 1st integer register
            OpCode::PUSHI2 => 0x69,             // Push from 2nd integer register
            OpCode::PUSHI3 => 0x6A,             // Push from 3rd integer register
            OpCode::PUSHF0 => 0x6B,             // Push from 0th float register
            OpCode::PUSHF1 => 0x6C,             // Push from 1st float register
            OpCode::PUSHF2 => 0x6D,             // Push from 2nd float register
            OpCode::PUSHF3 => 0x6E,             // Push from 3rd float register
            OpCode::PUSHF4 => 0x6F,             // Push from 4th float register
            OpCode::PUSHF5 => 0x70,             // Push from 5th float register
            OpCode::PUSHF6 => 0x71,             // Push from 6th float register
            OpCode::PUSHF7 => 0x72,             // Push from 7th float register
            OpCode::PUSHF8 => 0x73,             // Push from 8th float regsiter
            OpCode::PUSHF9 => 0x74,             // Push from 9th float register
            OpCode::PUSHAINTER { .. } => 0xF5,  // ???
            OpCode::SYSTEM { .. } => 0xF6,      // ???
            OpCode::REQWAIT => 0x77,            // ???
            OpCode::PREQWAIT => 0x78,           // ???
            OpCode::REQCHG => 0x79,             // ???
            OpCode::ACTREQ => 0x7A,             // ???
        }
    }

    pub fn instruction(&self) -> Vec<u8> {
        match self {
            OpCode::OPLOR { lhs, rhs }
            | OpCode::OPLAND { lhs, rhs }
            | OpCode::OPOR { lhs, rhs }
            | OpCode::OPEOR { lhs, rhs }
            | OpCode::OPAND { lhs, rhs }
            | OpCode::OPEQ { lhs, rhs }
            | OpCode::OPNE { lhs, rhs }
            | OpCode::OPGTU { lhs, rhs }
            | OpCode::OPLSU { lhs, rhs }
            | OpCode::OPGT { lhs, rhs }
            | OpCode::OPLS { lhs, rhs }
            | OpCode::OPGTEU { lhs, rhs }
            | OpCode::OPLSEU { lhs, rhs }
            | OpCode::OPGTE { lhs, rhs }
            | OpCode::OPLSE { lhs, rhs }
            | OpCode::OPBON { lhs, rhs }
            | OpCode::OPBOFF { lhs, rhs }
            | OpCode::OPSLL { lhs, rhs }
            | OpCode::OPSRL { lhs, rhs }
            | OpCode::OPADD { lhs, rhs }
            | OpCode::OPSUB { lhs, rhs }
            | OpCode::OPMUL { lhs, rhs }
            | OpCode::OPDIV { lhs, rhs }
            | OpCode::OPMOD { lhs, rhs } => {
                let mut v = vec![];
                for op in *lhs.to_owned() {
                    v.extend(op.instruction());
                }
                for op in *rhs.to_owned() {
                    v.extend(op.instruction());
                }
                v.push(self.opcode());
                v
            },
            OpCode::OPNOT { operand }
            | OpCode::OPUMINUS { operand }
            | OpCode::OPBNOT { operand } => {
                let mut v = vec![];
                for op in *operand.to_owned() {
                    v.extend(op.instruction());
                }
                v.push(self.opcode());
                v
            },
            OpCode::PUSHV(id)
            | OpCode::POPV(id)
            | OpCode::POPVL(id)
            | OpCode::PUSHVP(id) => {
                let mut v = vec![self.opcode()];
                v.extend(id.to_le_bytes());
                v
            },
            OpCode::PUSHAR { id, idx }
            | OpCode::POPAR { id, idx }
            | OpCode::POPARL { id, idx }
            | OpCode::PUSHARP { id, idx } => {
                let mut v = vec![];
                v.extend(idx.instruction());
                v.push(self.opcode());
                v.extend(id.to_le_bytes());
                v
            },
            OpCode::PUSHI(id)
            | OpCode::PUSHF(id) => {
                let mut v = vec![self.opcode()];
                v.extend(id.to_le_bytes());
                v
            },
            OpCode::PUSHII(value) => {
                let mut v = vec![self.opcode()];
                v.extend(value.to_le_bytes());
                v
            },
            OpCode::JMP(id)
            | OpCode::CJMP(id)
            | OpCode::NCJMP(id)
            | OpCode::POPXJMP(id)
            | OpCode::POPXCJMP(id)
            | OpCode::POPXNCJMP(id)
            | OpCode::JSR(id) => {
                let mut v = vec![self.opcode()];
                v.extend(id.to_le_bytes());
                v
            },
            OpCode::CALL { addr, args }
            | OpCode::CALLPOPA { addr, args } => {
                let mut v = vec![];
                for arg in args {
                    v.extend(arg.instruction());
                }
                v.push(self.opcode());
                v.extend(addr.to_le_bytes());
                v
            },
            OpCode::PUSHFIX(unk) => {
                let mut v = vec![self.opcode()];
                v.extend(unk.to_le_bytes());
                v
            },
            OpCode::PUSHAINTER(unk) => {
                let mut v = vec![self.opcode()];
                v.extend(unk.to_le_bytes());
                v
            },
            OpCode::SYSTEM(unk) => {
                let mut v = vec![self.opcode()];
                v.extend(unk.to_le_bytes());
                v
            },
            _ => vec![self.opcode()],
        }
    }
}
