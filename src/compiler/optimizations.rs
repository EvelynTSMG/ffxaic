use crate::{parser::ast, T};

use super::Compiler;

//TODO: Make more optimizations after the compiler actually works

impl Compiler {
    fn optimize_expression(&self, expr: ast::Expr) -> ast::Expr {
        let mut new_expr = expr.clone();
        match new_expr {
            // Nothing to optimize in literals and identifiers
            ast::Expr::Literal(_) | ast::Expr::Ident(_) => return expr,

            // Optimize individual args in function calls
            ast::Expr::FnCall { ref mut args, .. } => {
                for arg in args.iter_mut() {
                    *arg = Self::optimize_expression(self, arg.to_owned());
                }
            }

            ast::Expr::PrefixOp { op, ref expr } => {
                let op_1 = op;
                new_expr = match expr.as_ref() {
                    ast::Expr::PrefixOp { op, expr } => {
                        // Not operators are useless when doubled
                        // (normally !!x could convert from int to bool but this language doesn't have booleans per se)
                        // Same for unary minus operators
                        if (op_1 == T![!] || op_1 == T![~] || op_1 == T![-]) && op_1 == *op {
                            expr.as_ref().clone()
                        } else {
                            new_expr
                        }
                    }
                    _ => new_expr,
                }
            }

            ast::Expr::InfixOp {
                ref mut op,
                ref mut lhs,
                ref mut rhs,
            } => {
                let op_1 = op;
                match rhs.as_mut() {
                    ast::Expr::PrefixOp { op, expr } => {
                        // Subtraction of unary minus is just addition
                        if op_1 == &T![-] && op == &T![-] {
                            *op_1 = T![+];
                            *rhs = Box::new(expr.as_ref().to_owned());
                        }
                    }
                    _ => {}
                }
            }
            _ => todo!(),
        }

        new_expr
    }
}
