use std::fmt::Display;
use std::ops::Index;
use std::{fs, env, collections::HashMap};

use serde::Deserialize;
use serde::Deserializer;
use serde::de;
use serde::de::value::U16Deserializer;

use crate::parser::ast;

use self::ops::OpCode;

pub mod ops;
pub mod optimizations;
pub mod user_constants;

const FUNC_JSON: &str = "functions.json";
const ENUM_JSON: &str = "enums.json";

pub struct Variable {
    id: u16,
    name: String,
}

pub struct CompiledScript {
    pub bytes: Vec<u8>,
    pub entry_points: Vec<u32>,
    pub jump_points: Vec<u32>,
    pub int_constants: Vec<u32>,
    pub float_constants: Vec<f32>,
}

impl Display for CompiledScript {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let entry_points: Vec<String> = self.entry_points.iter().map(|n| format!("0x{:04X}", n)).collect();
        let jump_points: Vec<String> = self.jump_points.iter().map(|n| format!("0x{:04X}", n)).collect();
        let integers: Vec<String> = self.int_constants.iter().map(|n| format!("0x{:08X}", n)).collect();
        let floats: Vec<String> = self.float_constants.iter().map(|n| format!("{:.3}", n)).collect();
        let bytes: Vec<String> = self.bytes.iter().map(|n| format!("{:02X}", n)).collect();
        write!(f, "Entry points: {}\nJump points: {}\n\nInteger Table: {}\nFloat Table: {}\n\nCode: {}", entry_points.join(", "), jump_points.join(", "), integers.join(", "), floats.join(", "), bytes.join(" "))
    }
}

pub struct Compiler {
    items: Vec<ast::Item>,
    pub variables: Vec<Variable>,
    pub int_constants: Vec<u32>,
    pub float_constants: Vec<f32>,
    next_jump_id: u16,
    next_entry_point_id: u16,
}

impl Compiler {
    // fun fact: if the monster isn't poisoned, prePoisonTick happens asynchronously
    pub const BASE_FUNCS: [&str; 11] = ["init", "process", "onTurn", "preTurn", "onTargetted", "onHit", "onDeath", "onAttack", "prePoisonTick", "onUnk", "postPoisonTick"];

    pub fn new(items: Vec<ast::Item>) -> Compiler {
        Compiler {
            items,
            variables: vec![],
            int_constants: vec![],
            float_constants: vec![],
            next_jump_id: 0,
            next_entry_point_id: 0,
        }
    }

    pub fn optimize(&mut self, optimization_level: u32) {
        todo!()
    }

    pub fn parse_asm(self: &mut Compiler) -> Vec<ops::OpCode> {
        let mut parsed: Vec<ops::OpCode> = vec![];

        // Only functions can be items right now so we don't worry about this
        let mut functions = self.items.clone();

        // Only use appropriately called functions since creating our own is utterly impossible
        functions.retain(|x| {
            let ast::Item::Function { name, .. } = x;
            Compiler::BASE_FUNCS.contains(&name.as_str())
        });

        functions.sort_by(|a, b| {
            let ast::Item::Function { name, .. } = a;
            let name_a = name;
            let ast::Item::Function { name, .. } = b;
            return Compiler::BASE_FUNCS.iter().position(|&x| x == name_a).cmp(&Compiler::BASE_FUNCS.iter().position(|&x| x == name));
        });

        for function in functions {
            parsed.extend(function.ops(self))
        }

        parsed
    }

    pub fn compile(self: &Compiler, ops: Vec<ops::OpCode>) -> CompiledScript {
        let mut bytes: Vec<u8> = vec![];
        let mut entry_points: Vec<u32> = vec![];
        let mut jump_points: Vec<u32> = vec![];

        for op in ops {
            match op {
                OpCode::TAG(id) => {
                    while entry_points.len() <= id as usize {
                        entry_points.push(0xFFFF);
                    }

                    entry_points[id as usize] = bytes.len() as u32;
                },
                OpCode::LABEL(id) => {
                    while jump_points.len() <= id as usize {
                        jump_points.push(0);
                    }

                    jump_points[id as usize] = bytes.len() as u32;
                },
                _ => bytes.append(&mut op.instruction()),
            }
        }

        CompiledScript {
            bytes,
            entry_points,
            jump_points,
            int_constants: self.int_constants.clone(),
            float_constants: self.float_constants.clone(),
        }
    }

    pub fn int(self: &Compiler, val: u32) -> u16 {
        self.int_constants.iter().position(|&x| x == val as u32).unwrap() as u16
    }

    pub fn float(self: &Compiler, val: f32) -> u16 {
        self.float_constants.iter().position(|&x| x == val as f32).unwrap() as u16
    }

    pub fn var(self: &Compiler, var_name: &str) -> u16 {
        self.variables.iter().position(|x| x.name == var_name).unwrap_or_else(|| panic!("Tried to use nonexistant variable `{}`", var_name)) as u16
    }

    pub fn new_var(self: &mut Compiler, var_name: &str) {
        if self.variables.iter().find(|&x|  x.name == *var_name).is_none() {
            self.variables.push(Variable {
                id: self.variables.len() as u16,
                name: var_name.to_string(),
            });
        } else {
            panic!("Tried to create new variables with duplicate name '{}'", var_name)
        }
    }

    pub fn next_jump(self: &mut Compiler) -> u16 {
        let jump_id = self.next_jump_id;
        self.next_jump_id += 1;
        return jump_id;
    }

    pub fn next_entry_point(self: &mut Compiler) -> u16 {
        let entry_point_id = self.next_entry_point_id;
        self.next_entry_point_id += 1;
        return entry_point_id;
    }
}
