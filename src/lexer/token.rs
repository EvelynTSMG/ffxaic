use std::{
    fmt::{self, Display},
    ops::{Index, Range},
};

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone)]
pub enum TokenKind {
    // Single characters
    Plus,
    Minus,
    Times,
    Slash,
    Modulo,
    Caret,
    Eq,
    Dot,
    Comma,
    Underscore,
    Bang,
    Tilde,
    Ampersand,
    Bar,
    Colon,
    SemiColon,
    // Brackets
    LAngle,
    RAngle,
    LSquare,
    RSquare,
    LBrace,
    RBrace,
    LParen,
    RParen,
    // Multiple characters
    Comment,
    Int,
    Float,
    String,
    Identifier,
    KeywordLet,
    KeywordFn,
    KeywordIf,
    KeywordElse,
    KeywordMatch,
    KeywordReturn,
    // Operators
    Arrow,
    FatArrow,
    DoubleColon,
    And,
    Or,
    BOff,
    BOn,
    Sll,
    Srl,
    Eqq,
    Neq,
    UGe,
    ULe,
    Geq,
    Leq,
    UGeq,
    ULeq,
    // Misc,
    Error,
    Whitespace,
    Eof,
}

#[macro_export]
macro_rules! T {
    [+] => {
        $crate::lexer::TokenKind::Plus
    };
    [-] => {
        $crate::lexer::TokenKind::Minus
    };
    [*] => {
        $crate::lexer::TokenKind::Times
    };
    [/] => {
        $crate::lexer::TokenKind::Slash
    };
    [%] => {
        $crate::lexer::TokenKind::Modulo
    };
    [^] => {
        $crate::lexer::TokenKind::Caret
    };
    [=] => {
        $crate::lexer::TokenKind::Eq
    };
    [.] => {
        $crate::lexer::TokenKind::Dot
    };
    [,] => {
        $crate::lexer::TokenKind::Comma
    };
    [_] => {
        $crate::lexer::TokenKind::Underscore
    };
    [!] => {
        $crate::lexer::TokenKind::Bang
    };
    [~] => {
        $crate::lexer::TokenKind::Tilde
    };
    [&] => {
        $crate::lexer::TokenKind::Ampersand
    };
    [|] => {
        $crate::lexer::TokenKind::Bar
    };
    [:] => {
        $crate::lexer::TokenKind::Colon
    };
    [;] => {
        $crate::lexer::TokenKind::SemiColon
    };
    [<] => {
        $crate::lexer::TokenKind::LAngle
    };
    [>] => {
        $crate::lexer::TokenKind::RAngle
    };
    ['['] => {
        $crate::lexer::TokenKind::LSquare
    };
    [']'] => {
        $crate::lexer::TokenKind::RSquare
    };
    ['{'] => {
        $crate::lexer::TokenKind::LBrace
    };
    ['}'] => {
        $crate::lexer::TokenKind::RBrace
    };
    ['('] => {
        $crate::lexer::TokenKind::LParen
    };
    [')'] => {
        $crate::lexer::TokenKind::RParen
    };
    [comment] => {
        $crate::lexer::TokenKind::Comment
    };
    [int] => {
        $crate::lexer::TokenKind::Int
    };
    [float] => {
        $crate::lexer::TokenKind::Float
    };
    [string] => {
        $crate::lexer::TokenKind::String
    };
    [ident] => {
        $crate::lexer::TokenKind::Identifier
    };
    [let] => {
        $crate::lexer::TokenKind::KeywordLet
    };
    [fn] => {
        $crate::lexer::TokenKind::KeywordFn
    };
    [if] => {
        $crate::lexer::TokenKind::KeywordIf
    };
    [else] => {
        $crate::lexer::TokenKind::KeywordElse
    };
    [match] => {
        $crate::lexer::TokenKind::KeywordMatch
    };
    [ret] => {
        $crate::lexer::TokenKind::KeywordReturn
    };
    [->] =>  {
        $crate::lexer::TokenKind::Arrow
    };
    [=>] => {
        $crate::lexer::TokenKind::FatArrow
    };
    [::] => {
        $crate::lexer::TokenKind::DoubleColon
    };
    [&&] => {
        $crate::lexer::TokenKind::And
    };
    [||] => {
        $crate::lexer::TokenKind::Or
    };
    [b!!] => {
        $crate::lexer::TokenKind::BOn
    };
    [b!] => {
        $crate::lexer::TokenKind::BOff
    };
    [<<] => {
        $crate::lexer::TokenKind::Sll
    };
    [>>] => {
        $crate::lexer::TokenKind::Srl
    };
    [==] => {
        $crate::lexer::TokenKind::Eqq
    };
    [!=] => {
        $crate::lexer::TokenKind::Neq
    };
    [u>] => {
        $crate::lexer::TokenKind::UGe
    };
    [u<] => {
        $crate::lexer::TokenKind::ULe
    };
    [>=] => {
        $crate::lexer::TokenKind::Geq
    };
    [<=] => {
        $crate::lexer::TokenKind::Leq
    };
    [u>=] => {
        $crate::lexer::TokenKind::UGeq
    };
    [u<=] => {
        $crate::lexer::TokenKind::ULeq
    };
    [error] => {
        $crate::lexer::TokenKind::Error
    };
    [ws] => {
        $crate::lexer::TokenKind::Whitespace
    };
    [EOF] => {
        $crate::lexer::TokenKind::Eof
    };
}

impl fmt::Display for TokenKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                // Single-char
                TokenKind::Plus => "+",
                TokenKind::Minus => "-",
                TokenKind::Times => "*",
                TokenKind::Slash => "/",
                TokenKind::Modulo => "%",
                TokenKind::Caret => "^",
                TokenKind::Eq => "=",
                TokenKind::Dot => ".",
                TokenKind::Comma => ",",
                TokenKind::Underscore => "_",
                TokenKind::Bang => "!",
                TokenKind::Tilde => "~",
                TokenKind::Ampersand => "&",
                TokenKind::Bar => "|",
                TokenKind::Colon => ":",
                TokenKind::SemiColon => ";",
                // Brackets
                TokenKind::LAngle => "<",
                TokenKind::RAngle => ">",
                TokenKind::LSquare => "[",
                TokenKind::RSquare => "]",
                TokenKind::LBrace => "{",
                TokenKind::RBrace => "}",
                TokenKind::LParen => "(",
                TokenKind::RParen => ")",
                // Multi-char
                TokenKind::Comment => "// Comment",
                TokenKind::Int => "int",
                TokenKind::Float => "float",
                TokenKind::String => "string",
                TokenKind::Identifier => "ident",
                TokenKind::KeywordLet => "let",
                TokenKind::KeywordFn => "fn",
                TokenKind::KeywordIf => "if",
                TokenKind::KeywordElse => "else",
                TokenKind::KeywordMatch => "match",
                TokenKind::KeywordReturn => "return",
                // Operators
                TokenKind::Arrow => "->",
                TokenKind::FatArrow => "=>",
                TokenKind::DoubleColon => "::",
                TokenKind::And => "&&",
                TokenKind::Or => "||",
                TokenKind::BOn => "b!",
                TokenKind::BOff => "b!!",
                TokenKind::Sll => "<<",
                TokenKind::Srl => ">>",
                TokenKind::Eqq => "==",
                TokenKind::Neq => "!=",
                TokenKind::UGe => "u>",
                TokenKind::ULe => "u<",
                TokenKind::Geq => ">=",
                TokenKind::Leq => "<=",
                TokenKind::UGeq => "u>=",
                TokenKind::ULeq => "u<=",
                // Misc
                TokenKind::Error => "<?>",
                TokenKind::Whitespace => "<WS>",
                TokenKind::Eof => "<EOF>",
            }
        )
    }
}

// copyable Range
#[derive(Eq, PartialEq, Clone, Copy, Hash, Default, Debug)]
pub struct Span {
    pub start: u32, // incl.
    pub end: u32,   // excl.
}

impl From<Span> for Range<usize> {
    fn from(span: Span) -> Self {
        span.start as usize..span.end as usize
    }
}

impl Display for Span {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.start, self.end)
    }
}

impl From<Range<usize>> for Span {
    fn from(range: Range<usize>) -> Self {
        Self {
            start: range.start as u32,
            end: range.end as u32,
        }
    }
}

impl Index<Span> for str {
    type Output = str;

    fn index(&self, index: Span) -> &Self::Output {
        &self[Range::<usize>::from(index)]
    }
}

#[derive(Eq, PartialEq, Copy, Clone, Hash)]
pub struct Token {
    pub kind: TokenKind,
    pub span: Span,
}

impl Token {
    pub fn len(&self) -> usize {
        (self.span.end - self.span.start) as usize
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn text<'input>(&self, input: &'input str) -> &'input str {
        &input[self.span]
    }
}

impl fmt::Debug for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{:?} - <{}, {}>",
            self.kind, self.span.start, self.span.end
        )
    }
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.kind)
    }
}
