use remiem::lexer::Lexer;

use std::time::Duration;
use unindent::unindent;

use criterion::{criterion_group, criterion_main, BatchSize, Criterion, Throughput};

fn lex_function(c: &mut Criterion) {
    let input = r#"
        fn test(var: Type1, var2_: bool) {
            let x = "string content \" test" + 7 / 27.3e-2 << 4;
            let chars = x.chars();
            if let Some(c) = chars.next() {
                x = x + c;
            } else if !var2_ {
                x = x + ",";
            } else {
                match x {
                    1 = y if z != 2 => {
                        // No
                    },
                    1 = y if z == 2 => {
                        // yes :3
                    },
                    2 = y => {
                        z = y;
                    },
                    3 => {
                        z = 5;
                    }
                }
            }
        }
    "#;

    let input = unindent(input);
    bench_lexer(c, "function", input.as_str());
}

fn bench_lexer(c: &mut Criterion, name: &str, input: &str) {
    let mut group = c.benchmark_group("lexer");
    group.measurement_time(Duration::from_millis(7500));

    group.throughput(Throughput::Bytes(input.as_bytes().len() as u64));
    group.bench_with_input(name, input, |b, input| {
        b.iter_batched(
            || Lexer::new(input),
            |mut lexer| lexer.tokenize(),
            BatchSize::SmallInput,
        )
    });
    group.finish();
}

criterion_group!(benches, lex_function);
criterion_main!(benches);
