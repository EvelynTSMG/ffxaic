use std::time::Duration;

use criterion::{criterion_group, criterion_main, Criterion, Throughput};
use remiem::parser::Parser;
use unindent::unindent;

pub fn parse_file(c: &mut Criterion) {
    let input = r#"
        fn test(var: Type1, var2_: bool) {
            let x = "string content \" test" + 7 / 27.3e-2 << 4;
            let chars = x.chars();
            if Some(c) == chars.next() {
                x = x + c;
            } else if !var2_ {
                x = x + ",";
            } else {
                match x {
                    1 = y if z != 2 => {
                        // No
                    },
                    1 = y if z == 2 => {
                        // yes :3
                    },
                    2 = y => {
                        z = y;
                    },
                    3 => {
                        z = 5;
                    }
                }
            }
        }
    "#;

    let input = unindent(input);
    bench_parser(c, "file", input.as_str());
}

fn bench_parser(c: &mut Criterion, name: &str, input: &str) {
    let mut group = c.benchmark_group("parser");
    group.measurement_time(Duration::from_secs(10));

    group.throughput(Throughput::Bytes(input.as_bytes().len() as u64));
    group.bench_with_input(name, input, |b, input| {
        b.iter_with_setup(
            || Parser::new(input),
            |mut parser| {
                let _tree = parser.file();
            },
        )
    });
    group.finish();
}

criterion_group!(benches, parse_file);
criterion_main!(benches);
